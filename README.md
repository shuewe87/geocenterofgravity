# GeoCenterOfGravity
This repository contains an android project for an app which calculates the geographical center of gravity for a set of locations.

##How to build the app
* Checkout the repository and import it in Android Studio
* Set your Google Maps Api Key in the file /secure.properties
* Refresh gradle and build the app

##Install the app without building it from source
A compiled apk is available here: www.shuewe.de/GeoCenterOfGravity.apk
