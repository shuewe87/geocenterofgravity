package de.shuewe.geogravity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * The main activity.
 */
public class MainActivity extends Activity {

    /**Text to look for location. */
    private EditText m_locationName;

    /**Factor to be entered. */
    private EditText m_locationFactor;

    /**ListView which holds current list. */
    private ListView m_locationList;

    /**Location object list. */
    private List<LocationItem> m_itemList= new ArrayList<LocationItem>();

    /**Adapter for the list. */
    private ListAdapter m_adapter;

    /**Name list of locations to be used from adapter. */
    private List<String> m_locationNameList = new ArrayList<String>();

    /**The buttons. */
    private Button m_buttons[];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        m_adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, m_locationNameList);
        m_locationName = (EditText) findViewById(R.id.stadt);
        m_buttons = new Button[3];
        m_buttons[0] = (Button) findViewById(R.id.such);
        m_buttons[1] = (Button) findViewById(R.id.such);
        m_buttons[2] = (Button) findViewById(R.id.such);
        m_locationFactor = (EditText) findViewById(R.id.faktor);
        m_locationList = (ListView) findViewById(R.id.anzeige);

    }

    /**
     * En/Disables the burttons.
     *
     * @param enabled enable or disable
     */
    private void setButtonsEnabled(boolean enabled){
        for (int z = 0; z < 3; z++) {
            m_buttons[z].setEnabled(enabled);
            m_buttons[z].invalidate();
        }
    }

    /**
     * Adds the currently entered location. Called via xml layout.
     *
     * @param view view
     */
    public void addLocation(View view) {

        setButtonsEnabled(false);
        m_locationList.setLongClickable(false);

        LocationItem location = new LocationItem(this,m_locationName.getText().toString(),Double.valueOf(m_locationFactor.getText().toString()).doubleValue());

        if(location.isFound()) {

            m_itemList.add(location);
            m_locationNameList.add(location.getListString());

            m_locationList.setAdapter(m_adapter);
            m_locationList.invalidate();
            m_locationList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                public boolean onItemLongClick(AdapterView<?> arg0, View v, int index, long arg3) {
                    del(index);
                    return true;
                }
            });

        }
        m_locationName.setText("");
        m_locationFactor.setText("1");
        setButtonsEnabled(true);
        m_locationList.setLongClickable(true);
    }

    /**
     * Deletes location from list.
     *
     * @param number to be deleted
     */
    public void del(int number) {

        m_itemList.remove(number);
        m_locationNameList.remove(number);
        m_locationList.setAdapter(m_adapter);

    }

    /**
     * Deletes the whole list. Called from xml layout.
     *
     * @param view the view
     */
    public void deleteList(View view) {
        if (m_itemList.size() > 0) {
            m_locationNameList.clear();
            m_itemList.clear();
            m_locationList.setAdapter(m_adapter);
        }
    }

    /**
     * Shows the map. Called from xml layout.
     *
     * @param view the view
     */
    public void showMap(View view) {
        if (m_itemList.size() > 0) {
            ArrayList<String> listringlat = new ArrayList<String>();
            ArrayList<String> listringlong = new ArrayList<String>();
            ArrayList<String> listringName = new ArrayList<String>();
            ArrayList<String> listringFactor = new ArrayList<String>();

            for(LocationItem item :m_itemList){
                listringName.add(item.getName());
                listringlat.add(String.valueOf(item.getLat()));
                listringlong.add(String.valueOf(item.getLng()));
                listringFactor.add(String.valueOf(item.getFactor()));
            }

            Intent i = new Intent(this, MapCenter.class);
            i.putStringArrayListExtra("lat", listringlat);
            i.putStringArrayListExtra("lng", listringlong);
            i.putStringArrayListExtra("ort", listringName);
            i.putStringArrayListExtra("factor", listringFactor);
            startActivity(i);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}
