package de.shuewe.geogravity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.widget.TextView;

/**
 * The class of the map view.
 */
public class MapCenter extends FragmentActivity implements OnMapReadyCallback {

    /**Fragment which holds the map. */
	SupportMapFragment supportmapfragment;

	/**Fragment. */
	Fragment fragment;
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapcenter);
        FragmentManager fmanager = getSupportFragmentManager();
        fragment = fmanager.findFragmentById(R.id.map);
        supportmapfragment = (SupportMapFragment)fragment;
        supportmapfragment.getMapAsync(this);

    }
 
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap supportMap) {

        Intent i= getIntent();
        ArrayList<String> lat= i.getStringArrayListExtra("lat");
        ArrayList<String> lng = i.getStringArrayListExtra("lng");
        List<String> ort=i.getStringArrayListExtra("ort");
        List<String> fakstring= i.getStringArrayListExtra("factor");

        List<LocationItem> items= new ArrayList<LocationItem>();
        for(int z=0;z<lat.size();z++){
            items.add(new LocationItem(ort.get(z),Double.valueOf(lat.get(z)),Double.valueOf(lng.get(z)),Double.valueOf(fakstring.get(z))));
        }

        for(LocationItem item : items){
            if (item.getFactor()==1){
                supportMap.addMarker(new MarkerOptions().position(new LatLng(item.getLat(),item.getLng())).title(item.getName()));
            }
            else
            {
                supportMap.addMarker(new MarkerOptions().position(new LatLng(item.getLat(),item.getLng())).title(item.getName()+" x"+Double.toString(item.getFactor())));
            }
        }
        LocationItem centerLocation = LocationItem.getCenterLocation(items);
        TextView t= (TextView) findViewById(R.id.tview);

        supportMap.addMarker(new MarkerOptions().position(new LatLng(centerLocation.getLat(),centerLocation.getLng())).title(centerLocation.getName()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

        LatLngBounds bounds = getBounds(items);
        supportMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
        t.setText(centerLocation.getName());
    }

    /**
     * Gets bounds to show all markers.
     *
     * @param items to find bounds for
     * @return suitable LatLngBounds
     */
    private LatLngBounds getBounds(List<LocationItem> items){
        double minlat=items.get(0).getLat();
        double minlng=items.get(0).getLng();
        double maxlat=minlat;
        double maxlng=minlng;
        for (LocationItem item : items){
            if (item.getLat()>maxlat){maxlat=item.getLat();}
            if (item.getLng()>maxlng){maxlng=item.getLng();}
            if (item.getLat()<minlat){minlat=item.getLat();}
            if (item.getLng()<minlng){minlng=item.getLng();}
        }
        return new LatLngBounds(
                new LatLng(minlat,minlng),new LatLng(maxlat,maxlng));
    }
}