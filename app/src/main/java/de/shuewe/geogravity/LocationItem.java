package de.shuewe.geogravity;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Location item class.
 */
public class LocationItem {

    /**Latitude. */
    private double m_lat;

    /**Longitude. */
    private double m_lng;

    /**Name. */
    private String m_name;

    /**Factor. */
    private double m_factor;

    /**Location found. */
    private boolean m_found=false;

    /**
     * Public constructor.
     *
     * @param context app context
     * @param text to search location for
     * @param factor factor
     */
    public LocationItem(Context context, String text, double factor){
        final Geocoder geoc = new Geocoder(context);
        try {
            List<Address> listad = null;
            listad = geoc.getFromLocationName(text, 1);
            if (listad != null && listad.size() != 0) {
                m_lat = listad.get(0).getLatitude();
                m_lng = listad.get(0).getLongitude();
                m_factor = factor;
                m_name = text;
                m_found=true;
            }
        } catch (IOException e) {
            Log.d("Exception", "Exception");
        }
    }

    /**
     * Public constructor.
     *
     * @param name of location
     * @param lat latitude
     * @param lng longitude
     * @param factor factor
     */
    public LocationItem(String name, double lat, double lng,double factor){

        m_name=name;
        m_lat=lat;
        m_lng=lng;
        m_factor=factor;
    }

    /** *
     * Get List representation of location.
     *
     * @return string
     */
    public String getListString(){
       return "Ort: " + m_name + " Faktor: " +m_factor;
    }

    /**
     * Checks if location was found.
     *
     * @return boolean
     */
    public boolean isFound(){
        return m_found;
    }

    /**
     * Gets the longitude of the location.
     *
     * @return longitude
     */
    public double getLng(){return m_lng;}

    /**
     * Gets the latitude of the location
     *
     * @return latitude
     */
    public double getLat(){return m_lat;}

    /**
     * Gets the name of the location.
     *
     * @return location name
     */
    public String getName(){return m_name;}

    /**
     * Gets the factor of the location.
     *
     * @return factor
     */
    public double getFactor(){return m_factor;}

    /**
     * Estimates the center of a given list of locations.
     *
     * @param items to find center for
     * @return Center location
     */
    public static LocationItem getCenterLocation(List<LocationItem> items){
        double centerlat=0;
        double centerlng=0;
        double faksumme=0;

        double centerx=0, centery=0, centerz=0;
        for(LocationItem item:items){
            double sphereLat=Math.toRadians(90-item.getLat());
            double sphereLng=Math.toRadians(item.getLng());
            double xk =Math.sin(sphereLat)*Math.cos(sphereLng);
            double yk=Math.sin(sphereLat)*Math.sin(sphereLng);
            double zk=Math.cos(sphereLat);

            centerx+=xk*item.getFactor();
            centery+=yk*item.getFactor();
            centerz+=zk*item.getFactor();
            faksumme+=item.getFactor();
        }

        if (faksumme!=0){
            centerx = centerx / faksumme;
            centery = centery / faksumme;
            centerz = centerz / faksumme;

        }
        centerlat=90-Math.toDegrees(Math.acos(centerz/Math.sqrt(centerx*centerx+centery*centery+centerz*centerz)));
        centerlng=Math.toDegrees(Math.atan2(centery,centerx));
        DecimalFormat f= new DecimalFormat("#0.000");
        return new LocationItem("Center: Lat: "+f.format(centerlat)+" Lng: "+f.format(centerlng),centerlat,centerlng,1.0);
    }
}
